import { Particles } from './particles'

export type Milliseconds = number

export interface Gesture {
    timeOffset: number
    type?: string
    timeLine: { x: number, y: number, t: number }[]
}

export interface GestateConfig {
    tapthresh?: number,
    debug?: boolean,
    colors?: string[]
}

export class NewGestate {
    state: {
        isPlaying: boolean,
        isVisualising: boolean
        isRecording: boolean
    } = { isPlaying: false, isVisualising: false, isRecording: false }
    currentGesture: Gesture = null
    currentGestureType: string = null
    currentRecording: {
        startTime: Date,
        recTimeOffset: number,
        lastElapsed: Milliseconds
    } = null
    currentTouch: {
        movingPos: { x: number, y: number },
        trackTouchIdentifier: number
    } = {
            movingPos: null,
            trackTouchIdentifier: null
        }
    gestures: Gesture[] = []
    particles: Particles = new Particles()
    canvasElement: HTMLCanvasElement
    debug: boolean = false
    colors: string[] = null
    tapthresh: number
    touchStartTime: Date
    constructor(config?: GestateConfig) {
        this.tapthresh =  (config && config.tapthresh) ? config.tapthresh : 110
        this.debug = config && config.debug
        this.colors = (config && config.colors) ? config.colors : null
    }
    destroy() {
        this.particles.destroy()
        if (this.canvasElement) this.unregisterCanvas();
    }
    consoleLog(...args) {
        if (this.debug) {
            console.log('Gestate:', ...args)
        }
    }
    record(canvas: HTMLCanvasElement, gtype: string, time: number): void {
        if (this.state.isPlaying) {
            this.consoleLog('call stopPlay() first.')
            return
        }
        this.consoleLog('gestate start()')
        this.canvasElement = canvas
        this.state.isRecording = true
        this.currentGestureType = gtype
        this.currentRecording = {
            startTime: new Date(),
            recTimeOffset: time,
            lastElapsed: 0
        }
        this.registerCanvas()
        this.startVis()
    }
    stopRecord(): void {
        this.consoleLog('gestate stop()')
        this.state.isRecording = false
        this.stopVisualise()
        if (this.currentGesture) {
            this.finishCurrentGesture()
        }
        this.unregisterCanvas()
    }

    startVis() {
        this.currentTouch = {
            movingPos: null,
            trackTouchIdentifier: null
        }
        this.currentGesture = null
        this.consoleLog('starting visualisation')
        this.canvasElement.style['pointer-events'] = 'auto';
        this.state.isVisualising = true
        this.particles.begin(this.canvasElement, true, this.colors)
        this.displayTick()
    }
    stopVisualise(): void {
        this.consoleLog('stopping visualisation')
        this.canvasElement.style['pointer-events'] = 'none';
        this.particles.stop()
        this.state.isVisualising = false
    }
    displayTick(): void {
        if (this.currentTouch.movingPos) {
            this.particles.parp(this.currentTouch.movingPos.x, this.currentTouch.movingPos.y)
            // to here
            if (this.state.isRecording) {
                this.currentGesture.timeLine.push({
                    t: this.currentRecording.recTimeOffset + this.getElapsed() - this.currentGesture.timeOffset,
                    x: this.currentTouch.movingPos.x,
                    y: this.currentTouch.movingPos.y
                })
            }
        }
        if (this.state.isVisualising) {
            window.requestAnimationFrame(this.displayTick.bind(this))
        }
    }
    finishCurrentGesture(): void {
        // We record the current gesture regardless, but in recording mode, we store the gesture
        // to this.gestures.
        if (this.state.isRecording) {
            this.gestures.push(this.currentGesture)
        }
        if (this.isTap()) {
            this.particles.burst()
        }
        this.currentGesture = null
        this.currentTouch.movingPos = null
        this.particles.endParp()
    }

    isTap(): boolean {
        if (!this.currentGesture || !this.touchStartTime) return false
        const thisTime = new Date()
        const td = (thisTime.valueOf() - this.touchStartTime.valueOf())
        const x = this.currentGesture.timeLine.map(v => v.x)
        const y = this.currentGesture.timeLine.map(v => v.y)
        const maxx = x.reduce((a, b) => Math.max(a, b))
        const minx = x.reduce((a, b) => Math.min(a, b))
        const maxy = y.reduce((a, b) => Math.max(a, b))
        const miny = y.reduce((a, b) => Math.min(a, b))
        const dist = Math.sqrt(Math.pow(maxx - minx,2) + Math.pow(maxy - miny,2))
        return (td < this.tapthresh && dist < 0.02)
    }
    clearAll(): void {
        this.gestures = []
    }

    getGestures(): Gesture[] {
        return this.gestures
    }

    loadGestures(gestures: Gesture[]): void {
        this.consoleLog('loadGestures', gestures)
        this.gestures = gestures
    }

    playGestures(canvas: HTMLCanvasElement, time: Milliseconds) {
        this.canvasElement = canvas
        if (this.state.isRecording) {
            this.stopRecord()
        }
        if (this.state.isVisualising) {
            this.stopVisualise()
        }
        this.currentRecording = {
            startTime: new Date(),
            recTimeOffset: time,
            lastElapsed: time
        }
        this.currentTouch.movingPos = null
        if (!this.state.isPlaying) {
            this.state.isPlaying = true
            this.particles.begin(this.canvasElement, false, this.colors)
            this.playTick()
        }
    }
    stopPlay(): void {
        this.state.isPlaying = false
        this.particles.stop()
    }
    getCurrentGestureByTime(nt: number): Gesture {
        for (let f = this.gestures.length - 1; f >= 0; --f) {
            const st = this.gestures[f].timeOffset
            const et = st + this.gestures[f].timeLine[this.gestures[f].timeLine.length - 1].t
            if (nt >= st && nt <= et) {
                return this.gestures[f]
            }
        }
        return null
    }
    playTick() {
        const elapsed = this.getElapsed() + this.currentRecording.recTimeOffset
        const pGest = this.getCurrentGestureByTime(elapsed)
        if (pGest) {
            const tt = elapsed - pGest.timeOffset
            const oldt = this.currentRecording.lastElapsed - pGest.timeOffset
            for (const frame of pGest.timeLine) {
                if (frame.t > oldt && frame.t <= tt) {
                    this.particles.parp(frame.x, frame.y)
                }
            }
            this.currentRecording.lastElapsed = elapsed
        } else {
            this.particles.clearLastParp()
        }
        if (this.state.isPlaying) {
            window.requestAnimationFrame(this.playTick.bind(this))
        }
    }
    getElapsed(): Milliseconds {
        const thisTime = new Date()
        return (thisTime.valueOf() - this.currentRecording.startTime.valueOf())
    }

    registerCanvas() {
        this.canvasElement.addEventListener('touchstart', this.touchEvent.bind(this))
        this.canvasElement.addEventListener('touchmove', this.touchEvent.bind(this))
        this.canvasElement.addEventListener('touchend', this.touchEvent.bind(this))
    }
    unregisterCanvas() {
        this.canvasElement.removeEventListener('touchstart', this.touchEvent)
        this.canvasElement.removeEventListener('touchmove', this.touchEvent)
        this.canvasElement.removeEventListener('touchend', this.touchEvent)
        this.particles.destroy()
    }

    touchEvent(e: TouchEvent) {
        e.preventDefault()
        const getXYFromTouch = (touch: Touch): { x: number, y: number } => {
            const target = touch.target as Element
            const rect = target.getBoundingClientRect()
            const rx = touch.clientX - rect.left
            const ry = touch.clientY - rect.top
            return {
                x: rx / rect.width,
                y: ry / rect.height
            }
        }
        if (e.type === 'touchstart') {
            this.touchStartTime = new Date()
            const thisTouch: Touch = e.changedTouches[0]
            this.currentTouch.trackTouchIdentifier = thisTouch.identifier
            const touchPos = getXYFromTouch(thisTouch)
            this.currentGesture = {
                type: this.currentGestureType,
                timeOffset: this.currentRecording.recTimeOffset + this.getElapsed(),
                timeLine: [{
                    t: 0,
                    x: touchPos.x,
                    y: touchPos.y
                }]
            }
            this.currentTouch.movingPos = { x: touchPos.x, y: touchPos.y }
        } else if (e.type === 'touchmove') {
            const moveTouch = Array.from(e.changedTouches).find(x => x.identifier === this.currentTouch.trackTouchIdentifier)
            if (moveTouch) {
                const touchPos = getXYFromTouch(moveTouch)
                // move from here
                // this.currentGesture.timeLine.push({
                //     t: this.currentRecording.recTimeOffset + this.getElapsed() - this.currentGesture.timeOffset,
                //     x: touchPos.x,
                //     y: touchPos.y
                // })
                this.currentTouch.movingPos = { x: touchPos.x, y: touchPos.y }
            }
        } else if (e.type === 'touchend') {
            if (this.currentGesture || this.state.isVisualising) {
                const fidx = Array.from(e.changedTouches).findIndex(x => x.identifier === this.currentTouch.trackTouchIdentifier)
                if (fidx !== -1) {
                    this.finishCurrentGesture()
                }
            }
        }
    }


}