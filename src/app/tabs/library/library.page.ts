import { Component } from '@angular/core';
import { ISession } from 'src/app/interface';
import { Observable } from 'rxjs';
import { SessionsService } from 'src/app/services/sessions.service';

@Component({
    selector: 'app-library',
    templateUrl: 'library.page.html',
    styleUrls: ['library.page.scss']
})
export class LibraryPage {
    $sessionList: Observable<{ incomplete: ISession[], complete: ISession[] }>
    activated: Set<string> = new Set()
    showMenu: boolean = false
    openingmodal: boolean = false
    constructor(
        private sessions: SessionsService
    ) {
        this.$sessionList = this.sessions.observeSessions()
    }
    longpress(id: string) {
        if (this.activated.has(id)) {
            this.activated.delete(id)
        } else {
            this.activated.add(id)
        }
        this.showMenu = Array.from(this.activated.values()).length > 0
    }
    async tap(session: ISession) {
        if (this.openingmodal) return;
        this.openingmodal = true
        this.activated.clear()
        this.showMenu = false
        // do stuff to open a modal
        this.openingmodal = false
    }

}
