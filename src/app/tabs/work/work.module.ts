import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkPage } from './work.page';
import { Tab1PageRoutingModule } from './work-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    Tab1PageRoutingModule,
    ComponentsModule,
    FontAwesomeModule
  ],
  declarations: [WorkPage]
})
export class WorkPageModule {}
