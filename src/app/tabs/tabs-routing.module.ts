import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'x',
    component: TabsPage,
    children: [
      {
        path: 'work',
        loadChildren: () => import('./work/work.module').then(m => m.WorkPageModule)
      },
      {
        path: 'library',
        loadChildren: () => import('./library/library.module').then(m => m.LibraryPageModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule)
      },
      {
        path: '',
        redirectTo: '/x/work',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/x/work',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
