import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TagmodalPage } from './tagmodal.page';
import { ToolsService } from 'src/app/services/tools.service';

@Component({
  selector: 'app-taginator',
  templateUrl: './taginator.component.html',
  styleUrls: ['./taginator.component.scss'],
})
export class TaginatorComponent {
  @Input() tags: string[] = []
  @Input() readonly: boolean = false
  @Input() compact: boolean = false
  @Output() changed: EventEmitter<string[]> = new EventEmitter()
  constructor(
    private modalCtrl: ModalController,
    private tools: ToolsService
  ) { }

  async add() {
    const modal = await this.modalCtrl.create({
      component: TagmodalPage,
      componentProps: {
        tags: this.tags
      }
    })
    const res = await this.tools.showModal(modal)
    if (!res) {return}
    this.tags = res.tags
    this.changed.emit(res.tags)
  }

}
