import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { FormGroup, FormBuilder } from '@angular/forms';

export interface ITagChanges {
    added: string[]
    removed: string[]
    changed: boolean
}

@Component({
    selector: 'app-tagmodal',
    templateUrl: './tagmodal.page.html',
    styleUrls: ['./tagmodal.page.scss'],
})
export class TagmodalPage implements OnInit {
    @Input() tags: string[] = []
    changed: boolean = false
    dtags: { selected: boolean, source: string, tag: string }[] = []
    custom: string[] = []
    tQuery: FormGroup
    constructor(
        private modalCtrl: ModalController,
        private data: DataService,
        private formBuilder: FormBuilder
    ) {
        this.tQuery = this.formBuilder.group({
            text: [''],
        })
    }

    _hasTag(t: string): boolean {
        return this.dtags.findIndex(x => x.tag === t) !== -1
    }

    haveTags(): boolean {
        return this.dtags.length > 0
    }


    ngOnInit() {
        for (const t of this.tags) {
            this.dtags.push({ tag: t, selected: true, source: 'selected' })
        }
        for (let t of this.data.getUsedTags()) {
            if (!this._hasTag(t)) {
                this.dtags.push({ tag: t, selected: false, source: 'elsewhere' })
            }
        }
    }


    touchTag(tag: { selected: boolean, source: string, tag: string }) {
        if (tag.selected) {
            if (tag.source === 'typed') {
                const idx = this.dtags.findIndex(x => x.tag === tag.tag)
                this.dtags.splice(idx, 1)
            } else {
                tag.selected = false // will this work?
            }
        } else {
            tag.selected = true
        }
        this.changed = this.hasChanged()
    }

    touchCustom(tag: string) {
        this.dtags.push({ tag: tag, selected: true, source: 'typed' })
        const s = this.tQuery.value.text.split(/\s*[\s,#]\s*/).filter(Boolean)
        //this.tQuery = s.filter(x => x !== tag).join(' ')
        this.tQuery.controls.text.setValue(s.filter(x => x !== tag).join(' '))
        this.changed = this.hasChanged()
    }

    hasChanged(): boolean {
        for (let t of this.tags) {
            if (!this.dtags.find(x => x.tag === t).selected) {
                return true
            }
        }
        for (let t of this.dtags) {
            if (t.selected && this.tags.findIndex(x => x === t.tag) === -1) {
                return true
            }
        }
        return false
    }


    filterChoices() {
        this.custom = this.tQuery.value.text.split(/\s*[\s,#]\s*/).filter(Boolean)
    }

    close() {
        this.modalCtrl.dismiss()
    }


    acceptTags() {
        this.modalCtrl.dismiss({
            tags: this.dtags.filter(x => x.selected).map(x => x.tag)
        })
    }

}
