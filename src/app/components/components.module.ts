import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { KitchenSnippetComponent } from './kitchen-snippet/kitchen-snippet.component';
import { ProgressFilterComponent } from './progress-filter/progress-filter.component';
import { SwiperComponent } from './swiper/swiper.component';
import { TaginatorComponent } from './taginator/taginator.component';
import { TagmodalPage } from './taginator/tagmodal.page';
import { ReactiveFormsModule } from '@angular/forms';
import { LibrarySnippetComponent } from './library-snippet/library-snippet.component';

@NgModule({
  declarations: [KitchenSnippetComponent, LibrarySnippetComponent, ProgressFilterComponent, SwiperComponent, TaginatorComponent, TagmodalPage],
  imports: [
    IonicModule,
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [KitchenSnippetComponent, LibrarySnippetComponent, SwiperComponent, TaginatorComponent, TagmodalPage],
  entryComponents: [TagmodalPage]
})
export class ComponentsModule { }
