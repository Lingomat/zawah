import { Component, Input, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ISession } from 'src/app/interface';
import { SafeStyle } from '@angular/platform-browser';
import { SessionsService } from 'src/app/services/sessions.service';
import AlloyFinger from 'alloyfinger'

@Component({
    selector: 'app-library-snippet',
    styleUrls: ['./library-snippet.component.scss'],
    template: `
<div #snippetel class="snippet">
    <div class="photo" [style.background-image]="(imageStyle | async)?.style">
    </div>
    <div class="text">Expanded metadata and actions to go here</div>
</div>
  `
})
export class LibrarySnippetComponent implements OnInit, AfterViewInit, OnDestroy {
    // C O N S T R U C T O R
    @Input() session: ISession
    @Input() setac
    imageStyle: Promise<{style: SafeStyle, width: number, height: number}>
    alloy: AlloyFinger
    @ViewChild('snippetel') snippetElement: ElementRef
    @Output() longpress = new EventEmitter<void>()
    @Output() tap = new EventEmitter<void>()
    activated: Set<string> = new Set()
    constructor(
        private sesh: SessionsService) { }

    ngOnInit() {
        this.imageStyle = this.sesh.getImageStyleFromId(this.session.coverId)
    }

    ngAfterViewInit() {
        const div = this.snippetElement.nativeElement as HTMLDivElement
        this.alloy = new AlloyFinger(div, {
            tap: () => {
                this.tap.emit()
            },
            longTap: () => {
                this.longpress.emit()
            }
        })
    }

    ngOnDestroy() {
        this.alloy.destroy()
    }


}
