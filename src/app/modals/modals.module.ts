import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { DragsterComponent } from './dragster/dragster.component';
import { SortablejsModule } from 'ngx-sortablejs';
import { IgvComponent } from './igv/igv.component';
import { ComponentsModule } from '../components/components.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmComponent } from './confirm/confirm.component';
import { DescribeComponent } from './describe/describe.component';
import { FinishComponent } from './finish/finish.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DragsterComponent, IgvComponent, ConfirmComponent, DescribeComponent, FinishComponent],
  imports: [
    IonicModule,
    SortablejsModule,
    CommonModule,
    ReactiveFormsModule,
    ComponentsModule,
    FontAwesomeModule
  ],
  exports: []
})
export class ModalsModule { }
