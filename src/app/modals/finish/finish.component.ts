import { Component, OnInit, Input } from '@angular/core';
import { faCheck, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { ModalController, PopoverController } from '@ionic/angular';
import { ConfirmComponent } from '../confirm/confirm.component';
import { ToolsService } from 'src/app/services/tools.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SessionsService } from 'src/app/services/sessions.service';
import { ISession } from 'src/app/interface';

@Component({
    templateUrl: './finish.component.html',
    styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {
    @Input() session: ISession
    faCheck: IconDefinition = faCheck
    metadata: FormGroup;
    tags: string[] = []
    constructor(
        private modalCtrl: ModalController,
        private popCtrl: PopoverController,
        private tools: ToolsService,
        private formBuilder: FormBuilder,
        private sesh: SessionsService) {
            this.metadata = this.formBuilder.group({
                title: ['', Validators.required],
                description: ['']
            })
        }
    ngOnInit(): void {
    }

    async finish() {
        if (this.metadata.valid) {
            await this.sesh.updateSessionMetadata(this.session.id, this.metadata.value, this.tags)
            this.modalCtrl.dismiss()
        }
    }

    hasChanged() {
        return false // in case we want to check
    }

    async close(evt) {
        if (this.hasChanged()) {
            const res = await this.confirm('Quit and discard changes?', ['Yep', 'Nah'], evt)
            if (!res || res === 'Nah') {
                return
            }
        }
        this.modalCtrl.dismiss()
    }

    async confirm(message: string, options: string[], evt = null): Promise<string> {
        const popover = await this.popCtrl.create({
            component: ConfirmComponent,
            componentProps: {
                text: message,
                options
            }
        })
        return await this.tools.showPopover(popover) // shim which includes back button interception
    }


}
