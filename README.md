# Zawah

Zawah  is an app for documenting procedural knowledge with the Image-Gesture-Voice method. It is a tool to create documentary artefacts composed of a series of images with talk and touch gestures, rather like screencasting a series of slides. 

The app is a successor to the earlier Zahwa app, produced as part of the author's PhD thesis *Enabling Large-Scale Collaboration in Language Conservation*. The spelling change reflects native English-speakers pronunciation of Zahwa.

* Implemented as a pure PWA rather than a Cordova hybrid web app
* Primarily offline with private data synchronisation
* Removal of social collaboration and reliance and central data service

## Project status

This is a prototype reimplementation in contemporary web technologies. It is primarily intended to support interaction design of additional activities as part of a documentary *workflow*.

We have plans to use this tool in a number of research products at the Top End Language Lab at Charles Darwin University in Darwin, Australia. As such, the design goal is to create software sufficiently robust to be *useful*. This is not the same as as production-grade software.

## To-do

* Add respeaking task
* Add sharing
* Add data synchronisation -- Google Drive

## Getting Started

Install the current Node JS LTS version. `npm install -g @ionic/cli`. Clone this repo. `npm install` and then `ionic serve`.

Typically you will want to connect an Android phone via an ADB cable and port forward port 8100, and then remote open a Chrome tab to localhost:8100 to launch the app while ionic serve is running.

The project includes support for Firebase authentication. There must be a `/src/app/environments/environment.ts` file with Firebase config details obtained when setting up a new Firebase project and Web app. It sound look something like:

```
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "xxxxxxxx",
    authDomain: "project.firebaseapp.com",
    databaseURL: "https://project.firebaseio.com",
    projectId: "project",
    storageBucket: "project.appspot.com",
    messagingSenderId: "yyyyyy",
    appId: "1:zzzzzzz:web:zzzzz"
  }
};
```

It would be normal to have another firebase project for production, and have an `environment-prod.ts` which refers to the production (deployed) version.


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
